package tiendaonlinedemo.controller.facturacion;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import tiendaonlinedemo.controller.JSFUtil;
import tiendaonlinedemo.model.cliente.managers.ManagerCliente;
import tiendaonlinedemo.model.core.entities.CliCliente;
import tiendaonlinedemo.model.core.entities.CliPago;
import tiendaonlinedemo.model.core.entities.ThmEmpleado;
import tiendaonlinedemo.model.facturacion.dtos.DTODetallVenta;
import tiendaonlinedemo.model.facturacion.managers.ManagerVenta;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@SessionScoped
public class BeanVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	ManagerVenta managerVenta;
	@EJB
	ManagerCliente managerCliente;
	
	private List<CliCliente> listaClientes;
	private List<CliPago> listaPagos;
	private List<DTODetallVenta> listaVentas;
	
	private CliCliente cliente;
	private ThmEmpleado empleado;
	private CliCliente clienteSleccionado;
	private CliPago cliPago;
	private int pagoSeleccionado;
	private String cedulaCliente;
	private int id;
	private String nombre;
	private String correo;
	private boolean activo;
	private String direcCasa;
	private String direcTrabajo;
	private String num_contacto1;
	private String num_contacto2;
	private String tipo_pago;

	public BeanVenta() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listaClientes=managerVenta.findAllCliente();
		//listaPagos=managerCliente.findAllPagos();
	}

	public void buscarCliente() {
		try {
			cliente = managerVenta.findClienteByCedula(cedulaCliente);
			cedulaCliente=cliente.getCliCedula();
			id = cliente.getIdCliente();
			nombre = cliente.getCliNombre();
			correo = cliente.getCliCorreo();
			activo = cliente.getCliActivo();
			direcCasa="Dirección del Hohar: "+cliente.getCliDireccionCasa();
			direcTrabajo="Dirección del Trabajo: "+cliente.getCliDireccionTrabajo();
			num_contacto1="Número de Celular: "+cliente.getCliNumeroCelular();
			num_contacto2="Número Convencinoal: "+cliente.getCliNumeroConvencional();
			
			tipo_pago=cliente.getCliPago().getPagTipo();
			JSFUtil.crearMensajeINFO("Cliente encontrado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR("Cliente no encontrado");
			e.printStackTrace();
		}
	}
	
	
	
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("facturacion/tiendaonlinedemo.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/ssguzmani", "ssguzmani",
					"1003557715");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}
	
	public String getDirecCasa() {
		return direcCasa;
	}

	public void setDirecCasa(String direcCasa) {
		this.direcCasa = direcCasa;
	}

	public String getDirecTrabajo() {
		return direcTrabajo;
	}

	public void setDirecTrabajo(String direcTrabajo) {
		this.direcTrabajo = direcTrabajo;
	}

	public void actionListenerSeleccionadaCliente(CliCliente cli) {
		clienteSleccionado=cli;
	}
	
	
	public void actionListenerActualizarTipoPago() {
		try {
			//managerVenta.actualizarTipoPago(clienteSleccionado,cliente.getCliPago().getIdPago());
			JSFUtil.crearMensajeINFO("Tipo de Pago actualizado con éxito");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/*public  void actionListenerAgregarVenta() {
		try {
			List<DTODetallVenta> productos=new ArrayList<DTODetallVenta>();
			productos.add(new DTODetallVenta(1,3,20.3));
			productos.add(new DTODetallVenta(2,5,28));
			//managerVenta.crearCabeceraVenta(listaVentas, clienteSleccionado.getIdCliente(), empleado.getIdThmEmpleado());
			managerVenta.crearCabeceraVenta(productos, 2, 1);

			JSFUtil.crearMensajeINFO("Facturación exitosa");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}*/

	public List<CliCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<CliCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public String getCedulaCliente() {
		return cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getTipo_pago() {
		return tipo_pago;
	}

	public void setTipo_pago(String tipo_pago) {
		this.tipo_pago = tipo_pago;
	}

	public CliCliente getCliente() {
		return cliente;
	}

	public void setCliente(CliCliente cliente) {
		this.cliente = cliente;
	}

	public String getNum_contacto1() {
		return num_contacto1;
	}

	public void setNum_contacto1(String num_contacto1) {
		this.num_contacto1 = num_contacto1;
	}

	public String getNum_contacto2() {
		return num_contacto2;
	}

	public void setNum_contacto2(String num_contacto2) {
		this.num_contacto2 = num_contacto2;
	}

	public List<CliPago> getListaPagos() {
		return listaPagos;
	}

	public void setListaPagos(List<CliPago> listaPagos) {
		this.listaPagos = listaPagos;
	}

	public CliCliente getClienteSleccionado() {
		return clienteSleccionado;
	}

	public void setClienteSleccionado(CliCliente clienteSleccionado) {
		this.clienteSleccionado = clienteSleccionado;
	}

	public CliPago getCliPago() {
		return cliPago;
	}

	public void setCliPago(CliPago cliPago) {
		this.cliPago = cliPago;
	}

	public List<DTODetallVenta> getListaVentas() {
		return listaVentas;
	}

	public void setListaVentas(List<DTODetallVenta> listaVentas) {
		this.listaVentas = listaVentas;
	}

	public ThmEmpleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(ThmEmpleado empleado) {
		this.empleado = empleado;
	}


	public int getPagoSeleccionado() {
		return pagoSeleccionado;
	}

	public void setPagoSeleccionado(int pagoSeleccionado) {
		this.pagoSeleccionado = pagoSeleccionado;
	}
	
	
	
	
}
