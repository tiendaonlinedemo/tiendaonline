package tiendaonlinedemo.controller.cliente;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import tiendaonlinedemo.controller.JSFUtil;
import tiendaonlinedemo.model.cliente.managers.ManagerCliente;
import tiendaonlinedemo.model.core.entities.CliCliente;
import tiendaonlinedemo.model.core.entities.CliPago;

@Named
@SessionScoped
public class BeanCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerCliente managerCliente;
	private List<CliCliente> listaCliente;
	private List<CliPago> listaPago;

	private CliCliente clienteEdicion;
	private int idClienteEdicion;

	private String cli_cedula;
	private String cli_nombres;
	private String cli_correo;
	private boolean cli_activo;
	private String direccion_casa;
	private String direccion_trabajo;

	private String num_celular;
	private String num_convencional;
	private int idpagoSeleccionado;

	public BeanCliente() {

	}

	@PostConstruct
	public void inicializar() {
		listaCliente = managerCliente.findAllCliCliente();
		listaPago = managerCliente.findAllCliPago();
	}

	public void actionListenerInsertarCliente() {
		try {
			managerCliente.insertarCliente(cli_cedula, cli_nombres, cli_correo, cli_activo, direccion_casa,
					direccion_trabajo, num_celular, num_convencional, idpagoSeleccionado);
			listaCliente = managerCliente.findAllCliCliente();
			JSFUtil.crearMensajeINFO("Cliente ingresado correctamente.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerSeleccionarClienteEdicion(CliCliente cliente) {
		clienteEdicion = cliente;
		idClienteEdicion = cliente.getIdCliente();

	}

	public void actionListenerActualizarCliente() {
		managerCliente.actualizarCliente(clienteEdicion, idClienteEdicion);
		listaCliente = managerCliente.findAllCliCliente();
		JSFUtil.crearMensajeINFO("Cliente actualizado");
	}

	public void actionListenerEliminarCliente(int idcliente) {
		try {

			managerCliente.eliminarCliente(idcliente);
			listaCliente = managerCliente.findAllCliCliente();
			JSFUtil.crearMensajeINFO("Cliente eliminado correctamente");

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}
	
	public String actionReporte(){
		 Map<String,Object> parametros=new HashMap<String,Object>();
		 /*parametros.put("p_titulo_principal",p_titulo_principal);
		 parametros.put("p_titulo",p_titulo);*/
		 FacesContext context=FacesContext.getCurrentInstance();
		 ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		 String ruta=servletContext.getRealPath("cliente/reporteBGS.jasper");
		 System.out.println(ruta);
		 HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		 response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		 response.setContentType("application/pdf");
		 try {
		 Class.forName("org.postgresql.Driver");
		 Connection connection = null;
		 connection = DriverManager.getConnection(
		 "jdbc:postgresql://192.100.198.141:5432/ssguzmani","ssguzmani", "1003557715");
		 JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		 JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		 context.getApplication().getStateManager().saveView ( context ) ;
		 System.out.println("reporte generado.");
		 context.responseComplete();
		 } catch (Exception e) {
		 JSFUtil.crearMensajeERROR(e.getMessage());
		 e.printStackTrace();
		 }
		 return "";
		 }


	public List<CliCliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<CliCliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public List<CliPago> getListaPago() {
		return listaPago;
	}

	public void setListaPago(List<CliPago> listaPago) {
		this.listaPago = listaPago;
	}

	public int getIdpagoSeleccionado() {
		return idpagoSeleccionado;
	}

	public void setIdpagoSeleccionado(int idpagoSeleccionado) {
		this.idpagoSeleccionado = idpagoSeleccionado;
	}

	public String getCli_nombres() {
		return cli_nombres;
	}

	public void setCli_nombres(String cli_nombres) {
		this.cli_nombres = cli_nombres;
	}

	public String getCli_correo() {
		return cli_correo;
	}

	public void setCli_correo(String cli_correo) {
		this.cli_correo = cli_correo;
	}

	public boolean isCli_activo() {
		return cli_activo;
	}

	public void setCli_activo(boolean cli_activo) {
		this.cli_activo = cli_activo;
	}

	public CliCliente getClienteEdicion() {
		return clienteEdicion;
	}

	public void setClienteEdicion(CliCliente clienteEdicion) {
		this.clienteEdicion = clienteEdicion;
	}

	public int getIdClienteEdicion() {
		return idClienteEdicion;
	}

	public void setIdClienteEdicion(int idClienteEdicion) {
		this.idClienteEdicion = idClienteEdicion;
	}

	public String getCli_cedula() {
		return cli_cedula;
	}

	public void setCli_cedula(String cli_cedula) {
		this.cli_cedula = cli_cedula;
	}

	public String getDireccion_casa() {
		return direccion_casa;
	}

	public void setDireccion_casa(String direccion_casa) {
		this.direccion_casa = direccion_casa;
	}

	public String getDireccion_trabajo() {
		return direccion_trabajo;
	}

	public void setDireccion_trabajo(String direccion_trabajo) {
		this.direccion_trabajo = direccion_trabajo;
	}

	public String getNum_celular() {
		return num_celular;
	}

	public void setNum_celular(String num_celular) {
		this.num_celular = num_celular;
	}

	public String getNum_convencional() {
		return num_convencional;
	}

	public void setNum_convencional(String num_convencional) {
		this.num_convencional = num_convencional;
	}

	public void addMessage() {
		String summary = cli_activo ? "Activado" : "Desactivado";
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
	}

}