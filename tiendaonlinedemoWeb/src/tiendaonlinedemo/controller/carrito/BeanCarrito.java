package tiendaonlinedemo.controller.carrito;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import tiendaonlinedemo.controller.JSFUtil;
import tiendaonlinedemo.controller.facturacion.BeanVenta;
import tiendaonlinedemo.model.core.entities.CliCliente;
import tiendaonlinedemo.model.core.entities.CliPago;
import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;
import tiendaonlinedemo.model.facturacion.dtos.DTODetallVenta;
import tiendaonlinedemo.model.facturacion.managers.ManagerVenta;
import tiendaonlinedemo.model.carrito.managers.ManagerCarrito;

@Named
@SessionScoped
public class BeanCarrito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCarrito managerCarrito;
	@EJB
	private ManagerVenta managerVenta;

	@Inject
	private BeanVenta ventaBean;

	private List<InvProducto> listaProducto;
	private List<InvProducto> carrito;
	private List<InvMarca> listaMarca;
	private List<InvColor> listaColor;
	private List<InvTipoProducto> listaTipo;
	private List<InvTalla> listaTalla;
	private List<CliCliente> listaCliente;

	public List<CliCliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<CliCliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	private InvProducto productoEdicion;
	private int idEdicion;
	private double totalCarrito;
	private double totalCOmpra;
	private int clienteSeleccionado;
	private CliCliente clienteEdicion;
	private int idPagoEdicion;

	private int marcaSeleccionada;
	private int colorSeleccionado;
	private int tallaSeleccionada;
	private int tipoProductoSeleccionado;
	private double pro_precio;
	private String imagen;
	private String descripcion;

	public BeanCarrito() {

	}

	@PostConstruct
	public void inicializar() {
		listaProducto = managerCarrito.findAllInvProducto();
		listaMarca = managerCarrito.findAllInvMarca();
		listaColor = managerCarrito.findAllInvColor();
		listaTalla = managerCarrito.findAllInvTalla();
		listaTipo = managerCarrito.findALlInvTipoProducto();
		carrito = new ArrayList<InvProducto>();
	}

	public void InsertarProductoMemoria(InvProducto p) {
		try {
			JSFUtil.crearMensajeINFO("Producto agregado .");
			carrito.add(p);
			totalCarrito = managerCarrito.calcularTotalCarrito(carrito);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * public void deleteMemoria(InvProducto index) { int cont = 0; for (InvProducto
	 * det : carrito) { if (det.getIdProducto() == index.getIdProducto()) {
	 * carrito.remove(cont); totalCarrito =
	 * managerCarrito.calcularTotalCarrito(carrito); break; } cont++; } }
	 */

	public void actionListenerSeleccionadoPago(CliCliente cliente) {
		clienteSeleccionado = cliente.getIdCliente();
	}

	public void actionListenerActualizarCliente() {
		try {
			managerVenta.actualizarTipoPago(clienteEdicion, idPagoEdicion);
			listaCliente = managerVenta.findAllCliente();
			JSFUtil.crearMensajeINFO("Cliente actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteMemoria(InvProducto index) {
		System.out.println("Producto elimiando correctamente. qsadas");

		int cont = 0;
		// borrar

		for (InvProducto det : carrito) {
			if (det.getIdProducto() == index.getIdProducto()) {
				carrito.remove(cont);
				totalCarrito = managerCarrito.calcularTotalCarrito(carrito);

				break;
			}
			cont++;

		}

	}

	public void actionListenerAgregarVenta() {
		try {
			List<DTODetallVenta> productos = new ArrayList<DTODetallVenta>();

			for (InvProducto det : carrito) {
				productos.add(new DTODetallVenta(det.getIdProducto(), 1, det.getProPrecio()));
			}
			managerVenta.crearCabeceraVenta(productos, ventaBean.getCliente().getIdCliente(), 2);
			JSFUtil.crearMensajeINFO("Facturación exitosa");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerClculoTotal() {
		try {
			managerCarrito.calcularTotalCompras(carrito, totalCOmpra);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * Metodo para eliminar un producto
	 */
	public void actionListenerEliminarProducto(int idproducto) {
		try {
			List<DTODetallVenta> productos = new ArrayList<DTODetallVenta>();

			for (InvProducto det : carrito) {
				productos.add(new DTODetallVenta(det.getIdProducto(), 1, det.getProPrecio()));
			}

			managerVenta.crearCabeceraVenta(productos, ventaBean.getCliente().getIdCliente(), 2);
			JSFUtil.crearMensajeINFO("Facturación exitosa");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * Metodo para seleccionar un producto
	 */

	/*
	 * public void actionListenerClculoTotal() { try {
	 * managerCarrito.calcularTotalCompras(carrito, totalCOmpra); } catch (Exception
	 * e) { JSFUtil.crearMensajeERROR(e.getMessage()); e.printStackTrace(); } }
	 */

	public void calcularTotalCompras() {
		double suma = managerCarrito.calcularTotalCarrito(carrito);
		double iva = 0;
		iva = suma * 0.12;
		totalCOmpra = iva + suma;
		System.out.println(totalCOmpra);
	}

	public void actionListenerCalcularToral() {
		try {
			managerCarrito.calcularTotalCompras(carrito, totalCOmpra);
			System.out.println(totalCOmpra);
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public BeanVenta getVentaBean() {
		return ventaBean;
	}

	public void setVentaBean(BeanVenta ventaBean) {
		this.ventaBean = ventaBean;
	}

	public double getTotalCOmpra() {
		return totalCOmpra;
	}

	public void setTotalCOmpra(double totalCOmpra) {
		this.totalCOmpra = totalCOmpra;
	}

	/**
	 * METODO PARA ACTUALIZAR UN PRODUCTO
	 */
	public void actionListenerActualizarProducto() {
		// managerCarrito.actualizarProduto(productoEdicion,idEdicion);
		listaProducto = managerCarrito.findAllInvProducto();
		JSFUtil.crearMensajeINFO("Producto editado");
	}

	public List<InvProducto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<InvProducto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public List<InvMarca> getListaMarca() {
		return listaMarca;
	}

	public void setListaMarca(List<InvMarca> listaMarca) {
		this.listaMarca = listaMarca;
	}

	public List<InvColor> getListaColor() {
		return listaColor;
	}

	public void setListaColor(List<InvColor> listaColor) {
		this.listaColor = listaColor;
	}

	public List<InvTipoProducto> getListaTipo() {
		return listaTipo;
	}

	public int getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(int clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

	public CliCliente getClienteEdicion() {
		return clienteEdicion;
	}

	public void setClienteEdicion(CliCliente clienteEdicion) {
		this.clienteEdicion = clienteEdicion;
	}

	public int getIdPagoEdicion() {
		return idPagoEdicion;
	}

	public void setIdPagoEdicion(int idPagoEdicion) {
		this.idPagoEdicion = idPagoEdicion;
	}

	public void setListaTipo(List<InvTipoProducto> listaTipo) {
		this.listaTipo = listaTipo;
	}

	public List<InvTalla> getListaTalla() {
		return listaTalla;
	}

	public void setListaTalla(List<InvTalla> listaTalla) {
		this.listaTalla = listaTalla;
	}

	public double getPro_precio() {
		return pro_precio;
	}

	public void setPro_precio(double pro_precio) {
		this.pro_precio = pro_precio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public InvProducto getProductoEdicion() {
		return productoEdicion;
	}

	public void setProductoEdicion(InvProducto productoEdicion) {
		this.productoEdicion = productoEdicion;
	}

	public int getIdEdicion() {
		return idEdicion;
	}

	public void setIdEdicion(int idEdicion) {
		this.idEdicion = idEdicion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public double getTotalCarrito() {
		return totalCarrito;
	}

	public void setTotalCarrito(double totalCarrito) {
		this.totalCarrito = totalCarrito;
	}

	public List<InvProducto> getCarrito() {
		return carrito;
	}

	public void setCarrito(List<InvProducto> carrito) {
		this.carrito = carrito;
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("carrito/reporte_Cabecera.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/ssguzmani", "ssguzmani",
					"1003557715");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}
}
