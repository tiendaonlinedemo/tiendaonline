package tiendaonlinedemo.controller.inventario;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;



import tiendaonlinedemo.controller.JSFUtil;
import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;
import tiendaonlinedemo.model.inventario.managers.ManagerAdministracion;


@Named
@SessionScoped
public class BeanAdministracion implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@EJB
	
	private ManagerAdministracion managerAdministracion;
	private List<InvProducto> listaProducto;
	private List<InvMarca> listaMarca;
	private List<InvColor> listaColor;
	
	private InvColor colorEdicion;
	private int idEdicionColor;
	
	private InvTalla tallaEdicion;
	private int idEdicionTalla;
	
	private InvMarca MarcaEdicion;
	private int idEdicionMarca;
	
	private InvTipoProducto TipoEdicion;
	private int idTipoEdicion;
	
	private List<InvTipoProducto> listaTipo;
	private List<InvTalla> listaTalla;
	//private boolean colapsado; 
	private String colorSeleccionado;
	private String marcaSeleccionada;
	private String tallaSeleccionada;
	private String tipoProductoSeleccionado;
	
	

	public BeanAdministracion() {
		
	}
	
	@PostConstruct
	public void inicializar() {
		listaProducto = managerAdministracion.findAllInvProducto();
		listaMarca = managerAdministracion.findAllInvMarca();
		listaColor = managerAdministracion.findAllInvColor();
		listaTalla = managerAdministracion.findAllInvTalla();
		listaTipo = managerAdministracion.findALlInvTipoProducto();
		
		
	}
	/*public void actionListenerColapsarPanel() {
		colapsado=!colapsado;
	}*/
	
	/////////////////////////////////-------------------------------TALLA---------------------------------------------------/////////////////////////////////////

	/**
	 * ACTION PARA INGRESAR UN PRODUCTO
	 */
	
	public void actionListenerInsertarTalla() {
		try {
			managerAdministracion.insetarTalla(tallaSeleccionada);
			listaTalla=managerAdministracion.findAllInvTalla();
			JSFUtil.crearMensajeINFO("Talla ingresada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA ELIMINAR UN PRODUCTO
	 */
	
	public void actionListenerEliminarTalla(int idTalla) {
		try {
			managerAdministracion.eliminarTalla(idTalla);
			listaTalla=managerAdministracion.findAllInvTalla();
			JSFUtil.crearMensajeINFO("Talla eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA SELECCIONAR UN COLOR
	 */
	public void actionListenerSeleccionarTalla(InvTalla talla) {
		tallaEdicion=talla;
		idEdicionTalla=talla.getIdTalla();
		
	}
	
	/**
	 * ACTION PARA ACTUALIZAR UN COLOR
	 */
	
	public void actionListenerActualizarTalla() {
		managerAdministracion.actualizarTalla(tallaEdicion, idEdicionTalla);
		listaTalla=managerAdministracion.findAllInvTalla();
		JSFUtil.crearMensajeINFO("Talla actualizada correctamente");
	}
	
	
	/////////////////////////////////-------------------------------COLOR---------------------------------------------------/////////////////////////////////////

	/*
	 * INSERTAR TALLA
	 * 
	 */
	public void actionListenerInsertarColor() {
		try {
			managerAdministracion.insertarColor(colorSeleccionado);
			listaColor=managerAdministracion.findAllInvColor();
			JSFUtil.crearMensajeINFO("Color ingresado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA ELIMINAR UN PRODUCTO
	 */
	
	public void actionListenerEliminarColor(int idColor) {
		try {
			managerAdministracion.eliminarColor(idColor);
			listaColor=managerAdministracion.findAllInvColor();
			JSFUtil.crearMensajeINFO("Color eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA SELECCIONAR UN COLOR
	 */
	public void actionListenerSeleccionarColor(InvColor color) {
		colorEdicion=color;
		idEdicionColor=color.getIdColor();
		
	}
	
	/**
	 * ACTION PARA ACTUALIZAR UN COLOR
	 */
	
	public void actionListenerActualizarColor() {
		managerAdministracion.actualizarColor(colorEdicion, idEdicionColor);
		listaColor=managerAdministracion.findAllInvColor();
		JSFUtil.crearMensajeINFO("Color actualizado correctamente");
	}
	
	
	
	/////////////////////////////////-------------------------------TIPO PRODUTO---------------------------------------------------/////////////////////////////////////

	public void actionListenerInsertarTipo() {
		try {
			managerAdministracion.insetarTipoProducto(tipoProductoSeleccionado);
			listaTipo=managerAdministracion.findALlInvTipoProducto();
			JSFUtil.crearMensajeINFO("Tipo producto ingresada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA ELIMINAR UN PRODUCTO
	 */
	
	public void actionListenerEliminarTipo(int idTipo) {
		try {
			managerAdministracion.eliminarTipoProducto(idTipo);
			listaTipo=managerAdministracion.findALlInvTipoProducto();
			JSFUtil.crearMensajeINFO("Tipo Producto eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA SELECCIONAR UN COLOR
	 */
	public void actionListenerSeleccionarTipo(InvTipoProducto tipo) {
		TipoEdicion=tipo;
		idTipoEdicion=tipo.getIdTipoProducto();
		
	}
	
	/**
	 * ACTION PARA ACTUALIZAR UN COLOR
	 */
	
	public void actionListenerActualizarTipo() {
		managerAdministracion.actualizarTipoProducto(TipoEdicion, idTipoEdicion);
		listaTipo=managerAdministracion.findALlInvTipoProducto();
		JSFUtil.crearMensajeINFO("Tipo Producto actualizado correctamente");
	}
	
	
	/////////////////////////////////-------------------------------MARCA---------------------------------------------------/////////////////////////////////////

	public void actionListenerInsertarMarca() {
		try {
			managerAdministracion.insetarMarca(marcaSeleccionada);
			listaMarca=managerAdministracion.findAllInvMarca();
			JSFUtil.crearMensajeINFO("Marca ingresada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA ELIMINAR UN PRODUCTO
	 */
	
	public void actionListenerEliminarMarca(int idMarca) {
		try {
			managerAdministracion.eliminarMarca(idMarca);
			listaMarca=managerAdministracion.findAllInvMarca();
			JSFUtil.crearMensajeINFO("Marca eliminada correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * ACTION PARA SELECCIONAR UN COLOR
	 */
	public void actionListenerSeleccionarMarca(InvMarca marca) {
		MarcaEdicion=marca;
		idEdicionMarca=marca.getIdMarca();
		
	}
	
	/**
	 * ACTION PARA ACTUALIZAR UN COLOR
	 */
	
	public void actionListenerActualizarMarca() {
		managerAdministracion.actualizarMarca(MarcaEdicion, idEdicionMarca);
		listaMarca=managerAdministracion.findAllInvMarca();
		JSFUtil.crearMensajeINFO("Marca actualizada correctamente");
	}
	
	
	
	
	
	
	

	public List<InvProducto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<InvProducto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public List<InvMarca> getListaMarca() {
		return listaMarca;
	}

	public void setListaMarca(List<InvMarca> listaMarca) {
		this.listaMarca = listaMarca;
	}

	public List<InvColor> getListaColor() {
		return listaColor;
	}

	public void setListaColor(List<InvColor> listaColor) {
		this.listaColor = listaColor;
	}

	public InvColor getColorEdicion() {
		return colorEdicion;
	}

	public void setColorEdicion(InvColor colorEdicion) {
		this.colorEdicion = colorEdicion;
	}

	public int getIdEdicionColor() {
		return idEdicionColor;
	}

	public void setIdEdicionColor(int idEdicionColor) {
		this.idEdicionColor = idEdicionColor;
	}

	public List<InvTipoProducto> getListaTipo() {
		return listaTipo;
	}

	public void setListaTipo(List<InvTipoProducto> listaTipo) {
		this.listaTipo = listaTipo;
	}

	public List<InvTalla> getListaTalla() {
		return listaTalla;
	}

	public void setListaTalla(List<InvTalla> listaTalla) {
		this.listaTalla = listaTalla;
	}

	public String getColorSeleccionado() {
		return colorSeleccionado;
	}

	public void setColorSeleccionado(String colorSeleccionado) {
		this.colorSeleccionado = colorSeleccionado;
	}

	public String getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(String marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public String getTallaSeleccionada() {
		return tallaSeleccionada;
	}

	public void setTallaSeleccionada(String tallaSeleccionada) {
		this.tallaSeleccionada = tallaSeleccionada;
	}

	public String getTipoProductoSeleccionado() {
		return tipoProductoSeleccionado;
	}

	public void setTipoProductoSeleccionado(String tipoProductoSeleccionado) {
		this.tipoProductoSeleccionado = tipoProductoSeleccionado;
	}

	public InvTalla getTallaEdicion() {
		return tallaEdicion;
	}

	public void setTallaEdicion(InvTalla tallaEdicion) {
		this.tallaEdicion = tallaEdicion;
	}

	public int getIdEdicionTalla() {
		return idEdicionTalla;
	}

	public void setIdEdicionTalla(int idEdicionTalla) {
		this.idEdicionTalla = idEdicionTalla;
	}

	public InvMarca getMarcaEdicion() {
		return MarcaEdicion;
	}

	public void setMarcaEdicion(InvMarca marcaEdicion) {
		MarcaEdicion = marcaEdicion;
	}

	public int getIdEdicionMarca() {
		return idEdicionMarca;
	}

	public void setIdEdicionMarca(int idEdicionMarca) {
		this.idEdicionMarca = idEdicionMarca;
	}

	public InvTipoProducto getTipoEdicion() {
		return TipoEdicion;
	}

	public void setTipoEdicion(InvTipoProducto tipoEdicion) {
		TipoEdicion = tipoEdicion;
	}

	public int getIdTipoEdicion() {
		return idTipoEdicion;
	}

	public void setIdTipoEdicion(int idTipoEdicion) {
		this.idTipoEdicion = idTipoEdicion;
	}
	
	

	/*public boolean isColapsado() {
		return colapsado;
	}

	public void setColapsado(boolean colapsado) {
		this.colapsado = colapsado;
	}*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
