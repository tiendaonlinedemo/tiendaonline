package tiendaonlinedemo.controller.inventario;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import tiendaonlinedemo.controller.JSFUtil;
import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;
import tiendaonlinedemo.model.inventario.managers.ManagerInventario;

@Named
@SessionScoped
public class BeanInventario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerInventario managerInventario;
	private List<InvProducto> listaProducto;
	private List<InvMarca> listaMarca;
	private List<InvColor> listaColor;
	private List<InvTipoProducto> listaTipo;
	private List<InvTalla> listaTalla;
	private InvProducto productoEdicion;
	private int idEdicion;

	private int marcaSeleccionada;
	private int colorSeleccionado;
	private int tallaSeleccionada;
	private int tipoProductoSeleccionado;
	private double pro_precio;
	private String imagen;
	private int stock;
	private String descripcion;

	public BeanInventario() {

	}

	@PostConstruct
	public void inicializar() {
		listaProducto = managerInventario.findAllInvProducto();
		listaMarca = managerInventario.findAllInvMarca();
		listaColor = managerInventario.findAllInvColor();
		listaTalla = managerInventario.findAllInvTalla();
		listaTipo = managerInventario.findALlInvTipoProducto();
	}

	/**
	 * Metodo para insertar un producto
	 */
	public void actionListenerInsertarProducto() {
		try {
			managerInventario.insertarProducto(marcaSeleccionada, colorSeleccionado, tallaSeleccionada,
					tipoProductoSeleccionado, pro_precio, stock, descripcion, imagen);
			listaProducto = managerInventario.findAllInvProducto();
			JSFUtil.crearMensajeINFO("Producto ingresado correctamente.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * Metodo para eliminar un producto
	 */
	public void actionListenerEliminarProducto(int idproducto) {
		try {

			managerInventario.eliminarProducto(idproducto);
			listaProducto = managerInventario.findAllInvProducto();
			JSFUtil.crearMensajeINFO("Producto eliminado correctamente");

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}
	/*
	 * Metodo para seleccionar un producto
	 */

	public void actionListenerSeleccionarProductoEdicion(InvProducto producto) {
		productoEdicion = producto;
		idEdicion = producto.getIdProducto();

	}

	/**
	 * METODO PARA ACTUALIZAR UN PRODUCTO
	 */
	public void actionListenerActualizarProducto() {
		managerInventario.actualizarProduto(productoEdicion, idEdicion);
		listaProducto = managerInventario.findAllInvProducto();
		JSFUtil.crearMensajeINFO("Producto editado");
	}

	public List<InvProducto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<InvProducto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public List<InvMarca> getListaMarca() {
		return listaMarca;
	}

	public void setListaMarca(List<InvMarca> listaMarca) {
		this.listaMarca = listaMarca;
	}

	public List<InvColor> getListaColor() {
		return listaColor;
	}

	public void setListaColor(List<InvColor> listaColor) {
		this.listaColor = listaColor;
	}

	public List<InvTipoProducto> getListaTipo() {
		return listaTipo;
	}

	public void setListaTipo(List<InvTipoProducto> listaTipo) {
		this.listaTipo = listaTipo;
	}

	public List<InvTalla> getListaTalla() {
		return listaTalla;
	}

	public void setListaTalla(List<InvTalla> listaTalla) {
		this.listaTalla = listaTalla;
	}

	public int getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(int marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public int getColorSeleccionado() {
		return colorSeleccionado;
	}

	public void setColorSeleccionado(int colorSeleccionado) {
		this.colorSeleccionado = colorSeleccionado;
	}

	public int getTallaSeleccionada() {
		return tallaSeleccionada;
	}

	public void setTallaSeleccionada(int tallaSeleccionada) {
		this.tallaSeleccionada = tallaSeleccionada;
	}

	public int getTipoProductoSeleccionado() {
		return tipoProductoSeleccionado;
	}

	public void setTipoProductoSeleccionado(int tipoProductoSeleccionado) {
		this.tipoProductoSeleccionado = tipoProductoSeleccionado;
	}

	public double getPro_precio() {
		return pro_precio;
	}

	public void setPro_precio(double pro_precio) {
		this.pro_precio = pro_precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public InvProducto getProductoEdicion() {
		return productoEdicion;
	}

	public void setProductoEdicion(InvProducto productoEdicion) {
		this.productoEdicion = productoEdicion;
	}

	public int getIdEdicion() {
		return idEdicion;
	}

	public void setIdEdicion(int idEdicion) {
		this.idEdicion = idEdicion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/reporteProductos.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/ssguzmani", "ssguzmani",
					"1003557715");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

}
