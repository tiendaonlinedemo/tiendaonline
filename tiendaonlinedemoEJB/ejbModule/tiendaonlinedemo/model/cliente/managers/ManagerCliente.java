package tiendaonlinedemo.model.cliente.managers;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tiendaonlinedemo.model.core.entities.CliCliente;
import tiendaonlinedemo.model.core.entities.CliPago;

/**
 * Session Bean implementation class ManagerCliente
 */
@Stateless
@LocalBean
public class ManagerCliente {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerCliente() {

	}

	public List<CliCliente> findAllCliCliente() {
		TypedQuery<CliCliente> q = em.createQuery("SELECT c FROM CliCliente c", CliCliente.class);
		return q.getResultList();
	}

	public CliCliente findClienteById(int idcliente) {
		return em.find(CliCliente.class, idcliente);
	}

	public List<CliPago> findAllCliPago() {
		TypedQuery<CliPago> q = em.createQuery("SELECT c FROM CliPago c", CliPago.class);
		return q.getResultList();
	}

	public void insertarCliente(String cedula, String nombre, String correo, boolean activo, String direc_casa, String direc_trabajo, 
			String num_celular, String num_convencional , int idpago) {
		CliCliente cli = new CliCliente();
		CliPago pa = em.find(CliPago.class, idpago);
		cli.setCliCedula(cedula);
		cli.setCliNombre(nombre);
		cli.setCliCorreo(correo);
		cli.setCliActivo(activo);
		cli.setCliDireccionCasa(direc_casa);
		cli.setCliDireccionTrabajo(direc_trabajo);
		cli.setCliNumeroCelular(num_celular);
		cli.setCliNumeroConvencional(num_convencional);
		cli.setCliPago(pa);

		em.persist(cli);
	}

	public CliCliente actualizarCliente(CliCliente clienteEdicion, int idClienteEdicion) {
		CliCliente c = em.find(CliCliente.class, clienteEdicion.getIdCliente());
		c.setCliCedula(clienteEdicion.getCliCedula());
		c.setCliNombre(clienteEdicion.getCliNombre());
		c.setCliCorreo(clienteEdicion.getCliCorreo());
		c.setCliActivo(clienteEdicion.getCliActivo());
		c.setCliDireccionCasa(clienteEdicion.getCliDireccionCasa());
		c.setCliDireccionTrabajo(clienteEdicion.getCliDireccionTrabajo());
		c.setCliNumeroCelular(clienteEdicion.getCliNumeroCelular());
		c.setCliNumeroConvencional(clienteEdicion.getCliNumeroConvencional());
		//c.setCliPago(clienteEdicion.getCliPago());
		
		//CliPago p = em.find(CliPago.class, idClienteEdicion);
		//c.setCliPago(p);

		em.merge(c);
		return c;
	}

	public void eliminarCliente(int idcliente) {
		CliCliente cliente = findClienteById(idcliente);
		if (cliente != null) {
			em.remove(cliente);
		}
	}

}