package tiendaonlinedemo.model.facturacion.dtos;

public class DTODetallVenta {
	private int id_producto;
	private int cantidad;
	private double precio;
	
	
	
	public DTODetallVenta(int id_producto, int cantidad, double precio) {
		super();
		this.id_producto = id_producto;
		this.cantidad = cantidad;
		this.precio = precio;
	}
	
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
}
