package tiendaonlinedemo.model.facturacion.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tiendaonlinedemo.model.core.entities.CliCliente;
import tiendaonlinedemo.model.core.entities.CliPago;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.ThmEmpleado;
import tiendaonlinedemo.model.core.entities.VenCabecera;
import tiendaonlinedemo.model.core.entities.VenDetalle;
import tiendaonlinedemo.model.facturacion.dtos.DTODetallVenta;

/**
 * Session Bean implementation class ManagerVenta
 */
@Stateless
@LocalBean
public class ManagerVenta {

	@PersistenceContext
	EntityManager em;

	public ManagerVenta() {
	}

	public List<CliCliente> findAllCliente() {
		TypedQuery<CliCliente> q = em.createQuery("SELECT c FROM CliCliente c", CliCliente.class);
		return q.getResultList();
	}

	public List<CliPago> findAllPago() {
		TypedQuery<CliPago> q = em.createQuery("SELECT c FROM CliPago c", CliPago.class);
		return q.getResultList();
	}

	public CliCliente findClienteById(int id) {
		return em.find(CliCliente.class, id);
	}

	public CliPago findPagoById(int id) {
		return em.find(CliPago.class, id);
	}

	public CliCliente findClienteByCedula(String cedulaCliente) throws Exception {
		List<CliCliente> listaClientes = findAllCliente();
		CliCliente clienteNuevo = new CliCliente();
		boolean a = false;
		System.out.println(cedulaCliente);
		for (CliCliente cl : listaClientes) {
			if (cl.getCliCedula().equals(cedulaCliente)) {
				clienteNuevo = cl;
				a = true;
				System.out.println("cliente encontrado: " + cl.getCliNombre());
				break;
			}
		}
		if (!a)
			throw new Exception("Cliente no encontrado");

		return clienteNuevo;
	}

	public void actualizarTipoPago(CliCliente c, int idPagoEdicion) throws Exception {
		CliCliente md = em.find(CliCliente.class, c.getIdCliente());
		CliPago p = em.find(CliPago.class, idPagoEdicion);
		if (md == null)
			throw new Exception("NO existe el Cliente");
		
		md.setCliPago(p);
		
		em.merge(md);
	}

	public void crearCabeceraVenta(List<DTODetallVenta> listaDetalles, int id_cliente, int id_empleado) {
		CliCliente cliente = em.find(CliCliente.class, id_cliente);
		ThmEmpleado empleado = em.find(ThmEmpleado.class, id_empleado);
		double iva = 0;
		double total = 0;

		VenCabecera vc = new VenCabecera();
		vc.setCliCliente(cliente);
		vc.setThmEmpleado(empleado);
		vc.setVenCabFecha(new Date());

		List<VenDetalle> listaDetallesVentas = new ArrayList<VenDetalle>();
		vc.setVenDetalles(listaDetallesVentas);

		for (DTODetallVenta det : listaDetalles) {
			InvProducto producto = em.find(InvProducto.class, det.getId_producto());
			VenDetalle ventaDet = new VenDetalle();
			producto.setVenDetalles(listaDetallesVentas);

			ventaDet.setVenCabecera(vc);
			ventaDet.setInvProducto(producto);
			ventaDet.setVenDetCantidad(det.getCantidad());
			ventaDet.setVenDetPrecio(new BigDecimal(det.getPrecio()));
			listaDetallesVentas.add(ventaDet);
			iva += det.getPrecio() * 0.12;
			total = det.getPrecio()+iva;
		}
		vc.setVenCabTotal(new BigDecimal(total));
		vc.setVenCabIva(new BigDecimal(iva));
		em.persist(vc);

	}

	/*public void extraerDirecById(int id) throws Exception {
		List<CliDireccion> listaDirecciones = findAllDireccion();
		boolean a = false;
		System.out.println(id);
		for (CliDireccion cd : listaDirecciones) {
			if (cd.getCliCliente().getIdCliente().equals(id)) {
				a = true;
				System.out.println("Direccion encontrada: " + cd.getDirecCallePrincipal());
				/*
				 * direcCalleP=cd.getDirecCallePrincipal();
				 * direcCalleS=cd.getDirecCalleSecundaria(); direcNum=cd.getDirecNumero();
				 * direccion=direcCalleP+" "+direcNum+" y "+direcCalleS;
				 break;
			}
		}
		if (!a)
			throw new Exception("No existe direcciones");

		return direccion;
	}

	public void extraerContactoById(int id) throws Exception {
		List<CliContacto> listaContacto = findAllContacto();
		boolean a = false;
		String contacto="";
		String numCelular;
		String numConvencional;
		for (CliContacto cc : listaContacto) {
			if (cc.getCliCliente().getIdCliente().equals(id)) {
				a = true;
				System.out.println("Direccion encontrada: " + cc.getNumCelular());
				break;
			}
		}
		if (!a)
			throw new Exception("No existe direcciones");
	}*/

}
