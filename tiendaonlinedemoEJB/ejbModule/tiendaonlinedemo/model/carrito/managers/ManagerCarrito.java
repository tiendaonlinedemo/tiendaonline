package tiendaonlinedemo.model.carrito.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;

/**
 * Session Bean implementation class ManagerInventario
 */
@Stateless
@LocalBean
public class ManagerCarrito {

	@PersistenceContext
	private EntityManager em;
	List<InvProducto> listaProductos;

	public ManagerCarrito() {

	}

	public List<InvProducto> findAllInvProducto() {
		TypedQuery<InvProducto> q = em.createQuery("SELECT i FROM InvProducto i order by i.idProducto",
				InvProducto.class);
		return q.getResultList();
	}

	public InvProducto findProductoById(int idproducto) {
		return em.find(InvProducto.class, idproducto);
	}

	public List<InvColor> findAllInvColor() {
		TypedQuery<InvColor> q = em.createQuery("SELECT iv FROM InvColor iv", InvColor.class);
		return q.getResultList();
	}

	public List<InvMarca> findAllInvMarca() {
		TypedQuery<InvMarca> q = em.createQuery("SELECT im FROM InvMarca im", InvMarca.class);
		return q.getResultList();

	}

	public List<InvTalla> findAllInvTalla() {
		TypedQuery<InvTalla> q = em.createQuery("SELECT it FROM InvTalla it", InvTalla.class);
		return q.getResultList();
	}

	public List<InvTipoProducto> findALlInvTipoProducto() {
		TypedQuery<InvTipoProducto> q = em.createQuery("SELECT ip FROM InvTipoProducto ip", InvTipoProducto.class);
		return q.getResultList();
	}

	

	public void eliminarProducto(int idproducto) {
		InvProducto producto = findProductoById(idproducto);
		if (producto != null) {
			em.remove(producto);
		}
	}
	
	public double calcularTotalCarrito(List<InvProducto> carrito) {
		double suma = 0;
		for (InvProducto p : carrito)
			suma += p.getProPrecio();
		return suma;
	}
	
	public double calcularTotalCompras(List<InvProducto> carrito,double total) {
		double suma = calcularTotalCarrito(carrito);
		double iva = 0;
		iva=suma*0.12;
		total=iva+suma;
		System.out.println(total);
		return total;
	}
	
	
	
	
	public List<InvProducto> agregarProductoCarrito(List<InvProducto> carrito, InvProducto p) {
		if (carrito == null)
			carrito = new ArrayList<InvProducto>();
		carrito.add(p);
		return carrito;
	}

	public List<InvProducto> eliminarProductoCarrito(List<InvProducto> carrito, int codigoProducto) {
		if (carrito == null)
			return null;
		int i = 0;
		for (InvProducto p : carrito) {
			if (p.getIdProducto() == codigoProducto) {
				carrito.remove(i);
				break;
			}
			i++;
		}
		return carrito;
	}
}
