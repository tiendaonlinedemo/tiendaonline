package tiendaonlinedemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cli_cliente database table.
 * 
 */
@Entity
@Table(name="cli_cliente")
@NamedQuery(name="CliCliente.findAll", query="SELECT c FROM CliCliente c")
public class CliCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cliente", unique=true, nullable=false)
	private Integer idCliente;

	@Column(name="cli_activo", nullable=false)
	private Boolean cliActivo;

	@Column(name="cli_cedula", nullable=false, length=10)
	private String cliCedula;

	@Column(name="cli_correo", nullable=false, length=50)
	private String cliCorreo;

	@Column(name="cli_direccion_casa", length=200)
	private String cliDireccionCasa;

	@Column(name="cli_direccion_trabajo", length=200)
	private String cliDireccionTrabajo;

	@Column(name="cli_nombre", nullable=false, length=30)
	private String cliNombre;

	@Column(name="cli_numero_celular", length=10)
	private String cliNumeroCelular;

	@Column(name="cli_numero_convencional", length=9)
	private String cliNumeroConvencional;

	//bi-directional many-to-one association to CliPago
	@ManyToOne
	@JoinColumn(name="id_pago", nullable=false)
	private CliPago cliPago;

	public CliCliente() {
	}

	public Integer getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Boolean getCliActivo() {
		return this.cliActivo;
	}

	public void setCliActivo(Boolean cliActivo) {
		this.cliActivo = cliActivo;
	}

	public String getCliCedula() {
		return this.cliCedula;
	}

	public void setCliCedula(String cliCedula) {
		this.cliCedula = cliCedula;
	}

	public String getCliCorreo() {
		return this.cliCorreo;
	}

	public void setCliCorreo(String cliCorreo) {
		this.cliCorreo = cliCorreo;
	}

	public String getCliDireccionCasa() {
		return this.cliDireccionCasa;
	}

	public void setCliDireccionCasa(String cliDireccionCasa) {
		this.cliDireccionCasa = cliDireccionCasa;
	}

	public String getCliDireccionTrabajo() {
		return this.cliDireccionTrabajo;
	}

	public void setCliDireccionTrabajo(String cliDireccionTrabajo) {
		this.cliDireccionTrabajo = cliDireccionTrabajo;
	}

	public String getCliNombre() {
		return this.cliNombre;
	}

	public void setCliNombre(String cliNombre) {
		this.cliNombre = cliNombre;
	}

	public String getCliNumeroCelular() {
		return this.cliNumeroCelular;
	}

	public void setCliNumeroCelular(String cliNumeroCelular) {
		this.cliNumeroCelular = cliNumeroCelular;
	}

	public String getCliNumeroConvencional() {
		return this.cliNumeroConvencional;
	}

	public void setCliNumeroConvencional(String cliNumeroConvencional) {
		this.cliNumeroConvencional = cliNumeroConvencional;
	}

	public CliPago getCliPago() {
		return this.cliPago;
	}

	public void setCliPago(CliPago cliPago) {
		this.cliPago = cliPago;
	}

}