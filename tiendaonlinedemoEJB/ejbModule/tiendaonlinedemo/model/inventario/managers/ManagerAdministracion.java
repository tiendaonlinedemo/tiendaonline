package tiendaonlinedemo.model.inventario.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;

/**
 * Session Bean implementation class ManagerAdministracion
 */
@Stateless
@LocalBean
public class ManagerAdministracion {

    @PersistenceContext
    private EntityManager em;
	
    public ManagerAdministracion() {
       
    }
    public List<InvProducto> findAllInvProducto() {
		TypedQuery<InvProducto> q = em.createQuery("SELECT i FROM InvProducto i order by i.idProducto",
				InvProducto.class);
		return q.getResultList();
	}

	public InvProducto findProductoById(int idproducto) {
		return em.find(InvProducto.class, idproducto);
	}

	public List<InvColor> findAllInvColor() {
		TypedQuery<InvColor> q = em.createQuery("SELECT iv FROM InvColor iv order by iv.idColor", InvColor.class);
		return q.getResultList();
	}
	
	public InvColor findColorById(int idColor) {
		return em.find(InvColor.class, idColor);
	}

	public List<InvMarca> findAllInvMarca() {
		TypedQuery<InvMarca> q = em.createQuery("SELECT im FROM InvMarca im order by im.idMarca", InvMarca.class);
		return q.getResultList();
	}
	
	public InvMarca findMarcaById(int idMarca) {
		return em.find(InvMarca.class, idMarca);
	}

	public List<InvTalla> findAllInvTalla() {
		TypedQuery<InvTalla> q = em.createQuery("SELECT it FROM InvTalla it order by it.idTalla", InvTalla.class);
		return q.getResultList();
	}
	
	public InvTalla findTallaById(int idTalla) {
		return em.find(InvTalla.class, idTalla);
	}

	public List<InvTipoProducto> findALlInvTipoProducto() {
		TypedQuery<InvTipoProducto> q = em.createQuery("SELECT ip FROM InvTipoProducto ip order by ip.idTipoProducto", InvTipoProducto.class);
		return q.getResultList();
	}
	
	public InvTipoProducto findTipoById(int idTipoProducto) {
		return em.find(InvTipoProducto.class, idTipoProducto);
	}
	
	/**
	 * METODO PARA INSERTAR UN NUEVO COLOR
	 */
	public void insertarColor(String nombreColor) {
		//CREAMOS EL COLOR
		InvColor color = new InvColor();
		color.setColNombre(nombreColor);
		em.persist(color);
	}
	
	/**
	 * METODO PARA ELIMINAR UN COLOR
	 */
	public void eliminarColor(int idColor) {
		InvColor color = findColorById(idColor);
		if(color !=null) {
			em.remove(color);
		}
	}
	
	/**
	 * METODO PARA ACTUALIZAR UN COLOr
	 */
	public InvColor actualizarColor(InvColor colorEdicion, int idColorEdicion) {
		InvColor c = em.find(InvColor.class, colorEdicion.getIdColor());
		c.setColNombre(colorEdicion.getColNombre());
		em.merge(c);
		return c;
	}
	

	/**
	 * METODO PARA INSERTAR UNA NUEVA MARCA
	 */
	
	public void insetarMarca(String nombreMarca) {
		//CREAMOS LA MARCA
		InvMarca marca = new InvMarca();
		marca.setMarNombre(nombreMarca);
		em.persist(marca);
		
	}
	
	/**
	 * METODO PARA ELIMINAR UNA MARCA
	 */
	public void eliminarMarca(int idMarca) {
		InvMarca marca = findMarcaById(idMarca);
		if(marca !=null) {
			em.remove(marca);
		}
	}
	
	/**
	 * METODO PARA ACTUALIZAR UNA MARCA
	 */
	public InvMarca actualizarMarca(InvMarca marcaEdicion, int idMarcaEdicion) {
		InvMarca m = em.find(InvMarca.class, marcaEdicion.getIdMarca());
		m.setMarNombre(marcaEdicion.getMarNombre());
		em.merge(m);
		return m;
	}
	
	/**
	 * METODO PARA INSERTAR UNA NUEVO TIPO DE PRODUCTO
	 */
	
	public void insetarTipoProducto(String nombreTipoProducto) {
		//CREAMOS LA MARCA
		InvTipoProducto tipoProducto = new InvTipoProducto();
		tipoProducto.setTipoDescripcion(nombreTipoProducto);
		em.persist(tipoProducto);
		
	}
	
	/**
	 * METODO PARA ELIMINAR UN TIPO DE PRODUCTO
	 */
	public void eliminarTipoProducto(int idTipoProducto) {
		InvTipoProducto tipoProduto = findTipoById(idTipoProducto);
		if(tipoProduto !=null) {
			em.remove(tipoProduto);
		}
	}
	
	/**
	 * METODO PARA ACTUALIZAR UN TIPO PRODUCTO
	 */
	public InvTipoProducto actualizarTipoProducto(InvTipoProducto tipoProductoEdicion, int idTipoProductoEdicion) {
		InvTipoProducto tp = em.find(InvTipoProducto.class, tipoProductoEdicion.getIdTipoProducto());
		tp.setTipoDescripcion(tipoProductoEdicion.getTipoDescripcion());
		em.merge(tp);
		return tp;
	}
	
	/**
	 * METODO PARA INSERTAR UNA NUEVA TALLA
	 */
	
	public void insetarTalla(String nombreTalla) {
		//CREAMOS LA MARCA
		InvTalla talla = new InvTalla();
		talla.setTallDescripcion(nombreTalla); 
		em.persist(talla);
		
	}
	
	/**
	 * METODO PARA ELIMINAR UNA TALLA
	 */
	public void eliminarTalla(int idTalla) {
		InvTalla talla = findTallaById(idTalla);
		if(talla !=null) {
			em.remove(talla);
		}
	}
	
	/**
	 * METODO PARA ACTUALIZAR UNA TALLA
	 */
	public InvTalla actualizarTalla(InvTalla tallaEdicion, int idTallaEdicion) {
		InvTalla t = em.find(InvTalla.class, tallaEdicion.getIdTalla());
		t.setTallDescripcion(tallaEdicion.getTallDescripcion());
		em.merge(t);
		return t;
	}
	
	
	
	
	

    

}
