package tiendaonlinedemo.model.inventario.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tiendaonlinedemo.model.core.entities.InvColor;
import tiendaonlinedemo.model.core.entities.InvMarca;
import tiendaonlinedemo.model.core.entities.InvProducto;
import tiendaonlinedemo.model.core.entities.InvTalla;
import tiendaonlinedemo.model.core.entities.InvTipoProducto;

/**
 * Session Bean implementation class ManagerInventario
 */
@Stateless
@LocalBean
public class ManagerInventario {

	@PersistenceContext
	private EntityManager em;

	public ManagerInventario() {

	}

	public List<InvProducto> findAllInvProducto() {
		TypedQuery<InvProducto> q = em.createQuery("SELECT i FROM InvProducto i order by i.idProducto",
				InvProducto.class);
		return q.getResultList();
	}

	public InvProducto findProductoById(int idproducto) {
		return em.find(InvProducto.class, idproducto);
	}

	public List<InvColor> findAllInvColor() {
		TypedQuery<InvColor> q = em.createQuery("SELECT iv FROM InvColor iv", InvColor.class);
		return q.getResultList();
	}

	public List<InvMarca> findAllInvMarca() {
		TypedQuery<InvMarca> q = em.createQuery("SELECT im FROM InvMarca im", InvMarca.class);
		return q.getResultList();

	}

	public List<InvTalla> findAllInvTalla() {
		TypedQuery<InvTalla> q = em.createQuery("SELECT it FROM InvTalla it", InvTalla.class);
		return q.getResultList();
	}

	public List<InvTipoProducto> findALlInvTipoProducto() {
		TypedQuery<InvTipoProducto> q = em.createQuery("SELECT ip FROM InvTipoProducto ip", InvTipoProducto.class);
		return q.getResultList();
	}

	/**
	 * 
	 */

	public void insertarProducto(int id_marca, int id_color, int id_talla, int id_tipo_producto, double pro_precio,
			int pro_stock, String descripcion,String imagen) {
		InvProducto producto = new InvProducto();
		InvMarca im = em.find(InvMarca.class, id_marca);
		InvTalla it = em.find(InvTalla.class, id_talla);
		InvTipoProducto ip = em.find(InvTipoProducto.class, id_tipo_producto);
		InvColor ic = em.find(InvColor.class, id_color);

		// CREAMOS EL PRODUCTO
		producto.setProDescripcion(descripcion);
		producto.setInvMarca(im);
		producto.setInvColor(ic);
		producto.setInvTalla(it);
		producto.setInvTipoProducto(ip);
		producto.setProPrecio(pro_precio);
		producto.setProStock(pro_stock);
		producto.setProImagen(imagen);

		em.persist(producto);

	}

	/**
	 * 
	 */

	public InvProducto actualizarProduto(InvProducto productoEdicion, int idProductoEdicion) {
		InvProducto p = em.find(InvProducto.class, productoEdicion.getIdProducto());
		// p.setInvMarca(productoEdicion.getInvMarca());
		p.setProImagen(productoEdicion.getProImagen());
		p.setProDescripcion(productoEdicion.getProDescripcion());
		p.setProPrecio(productoEdicion.getProPrecio());
		p.setProStock(productoEdicion.getProStock());
		//InvMarca m = em.find(InvMarca.class, idProductoEdicion);
		//p.setInvMarca(m);
		em.merge(p);
		return p;
	}

	/**
	 * 
	 */

	public void eliminarProducto(int idproducto) {
		InvProducto producto = findProductoById(idproducto);
		if (producto != null) {
			em.remove(producto);
		}
	}
	
	public void disminuirStock(int idInvProducto, int cantidad ) throws Exception {
		InvProducto p = em.find(InvProducto.class, idInvProducto);
		if(p.getProStock()<cantidad) {
			throw new Exception("Stock insuficiente para la venta");
		}
		p.setProStock(p.getProStock()-cantidad);
		em.merge(p);
		
	}

}
